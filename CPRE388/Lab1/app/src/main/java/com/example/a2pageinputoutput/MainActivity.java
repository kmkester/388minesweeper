package com.example.a2pageinputoutput;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    public static String textVar = "";
    EditText input;
    Button send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.inputText);
        send = (Button) findViewById(R.id.sendButt);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textVar = input.getText().toString();
                startActivity(new Intent(MainActivity.this, MainActivity2.class));
            }
        });
    }


}