package com.example.stopwatch;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.os.Handler;
import android.os.SystemClock;

import java.util.logging.LogRecord;

public class MainActivityViewModel extends ViewModel {
    public static int timeMS = 1;

    private Handler handler = new Handler();
    private MutableLiveData<Integer> time = new MutableLiveData<Integer>();

    private boolean postAtTime(Runnable r, long uptimeMillis){
        return handler.postAtTime(r, uptimeMillis);
    }

    public int getTimeint(){

        return timeMS;
    }

    //SystemClock.uptimeMillis();
    public MutableLiveData<Integer> getTime(){
        return time;
    }


    public static void startTime(){

    }

    public static void resetTime(){
        //time = 0;
    }

    public static void pauseTime(){

    }
}
