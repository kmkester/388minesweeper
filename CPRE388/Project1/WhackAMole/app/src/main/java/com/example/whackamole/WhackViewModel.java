package com.example.whackamole;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;
import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import android.os.Handler;

public class WhackViewModel extends ViewModel {
    /**
     * The value of the high score, this loads from a shared preference in main.
     */
    public static int highScore = 0;

    /**
     * The value of the score of the last game played, this loads from a shared preference in main.
     */
    public static int lastScore = 0;

    /**
     * The value of the score of the current game.
     */
    public static int currScore = 0;

    /**
     * The value of the number of lives left of the current game.
     */
    public static int numLives = 3;
    static Random rand = new Random();
    private static int prev = 0;
    private static int random;

    /**
     *The value that the program needs to wait to gradually increase visibility change rate of moles.
     */
    public static int timeMS;

    static private boolean[] moleList = new boolean[9];


    //private List<Boolean> moleList = new ArrayList<Boolean>(Arrays.asList(new Boolean[9]));

    //private boolean m0Status = false, m1Status = false,m2Status = false,m3Status = false,m4Status = false,m5Status = false,m6Status = false,m7Status = false,m8Status = false;
//    private MutableLiveData<List<Integer>> moles;
//    public LiveData<List<Integer>> getMoles() {
//        if (users == null) {
//            users = new MutableLiveData<List<User>>();
//            loadUsers();
//        }
//        return users;
//    }

    /**
     * Sets the mole list array values to all false, helper method.
     */
    public static void falseMoleList(){
        for(int i = 0; i < moleList.length; i++){
            moleList[i] = false;
        }
    }

    /**
     * Getter method of the mole list, allows other classes to check its elements' values.
     * @param moleNum index at which to check mole status
     * @return the value of the array at index moleNum
     */
    public static boolean getMoleList(int moleNum){
        return moleList[moleNum];
    }

    private static int nextMole(){     //makes random different than prev
        random = rand.nextInt(9);
        while(prev == random){
            random = rand.nextInt(9);
        }
        prev = random;
        return random;
    }

    /**
     * uses nextMole() to generate a mole in a random location from 1-9 without repeating the
     * previous number.
     */
    public static void updateMoleList(){
        moleList[nextMole()] = true;
    }

    /**
     * Method to check if there is more than 1 mole onscreen at a time. The game activity uses this
     * method to handle if the player took too long to select a mole.
     * @return
     */
    public static boolean checkTwoMoles(){
        int moleCount = 0;
        for(int i = 0; i < moleList.length; i++){
            if(moleList[i]){
                moleCount++;
            }
        }
        if(moleCount > 1){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Subtracts 1 from lives upon missing a click for a mole.
     */
    public static void moleMissed(){
        numLives -= 1;
//        if (numLives == 0){
//
//        }
    }

    /**
     * Resets the values for the next game
     */
    public static void gameEnd(){
        lastScore = currScore;
        //save lastscore
        //save highscore
        currScore = 0;
        falseMoleList();
        numLives = 3;

    }

    /**
     * This method is called by the game activity whenever a mole is clicked. It sets the tick rate
     * by gradually increasing it. It also adds to score and updates the list.
     * @param moleNum the address of the mole that was clicked
     */
    public void moleClicked(int moleNum){
        //Handler handler = new Handler();

        currScore += 10;
        moleList[moleNum] = false;
        if(currScore<200){
            timeMS = 800-(2*currScore);
        }
        else{
            timeMS = 350;
        }
        updateMoleList();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                updateMoleList();
//            }
//        }, timeMS);   //5 seconds

    }
//    public int getCurrScore(){
//        return currScore;
//    }
//    public int getHighScore(){
//        return highScore;
//    }
//    public int getNumLives(){
//        return numLives;
//    }

}
