package com.example.minesweeper;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class Tile extends View {
    public static int NOTHING = 0;

    private int tileStatus;
    private boolean hasBomb;
    private boolean isShown;
    private boolean isFlagged;

    public Tile(int tileStatus, Context context){
        super(context);

        this.tileStatus = tileStatus;
        this.hasBomb = false;
        this.isShown = false;
        this.isFlagged = false;
    }
    public int getTileStatus(){
        return tileStatus;
    }
    public void setTileStatus(int tileStatus){
        this.tileStatus = tileStatus;
    }
    public void flag(){ isFlagged = true; }
    public void unFlag(){ isFlagged = false; }
    public void show(){ isShown = true; }
    public void giveBomb(){
        hasBomb = true;
    }
    public boolean hasBomb(){
        return hasBomb;
    }
    public boolean isShown(){ return isShown; }
    public boolean isFlagged(){
        return isFlagged;
    }
    //public Context getTileContext(){ return this.getContext(); };
}
